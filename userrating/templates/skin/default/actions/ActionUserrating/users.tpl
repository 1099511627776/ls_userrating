{include file="header.tpl" noSidebar="true"}
<div style="width:950px;">
    {literal}
	<script type="application/javascript">
		jQuery(function($){
			$('.datepicker').datepicker({
				dateFormat:'yy-mm-dd',
				showAnim:'slideDown'
			});		
		});
		function clearFilter(){
			jQuery('#from_date').val('clear');
			jQuery('#to_date').val('clear');
			return true;
		}
	</script>
	{/literal}
<form action="{router page='userrating'}admin/users" method="POST" enctype="multipart/form-data">
	<input type="text" name="fromDate" id="form_date" class="datepicker" ></input>
	<input type="text" name="toDate" id="to_date" class="datepicker" ></input>
	<input type="submit" name="Фильтровать"></input>
</form>
<table border=1 width="100%">
<tr>
	<th>ID</th>
	<th>Login</th>
	<th>Рейтинг</th>
	<th>Цінність</th>
	<th>%</th>
</tr>
{foreach from=$aUserStats item=oUser}
{if isset($oUser['user'])}
<tr>
	<td>{$oUser['user']->getId()}</td>
	<td>{$oUser['user']->getLogin()}</td>
	<td>{$oUser['user']->getRating()}</td>
	<td>{$oUser['norm_rating_fix']}</td>
	<td>{(100*$oUser['norm_rating_fix']/$totalSum)|string_format:'%.2f'}</td>
</tr>
{/if}
{/foreach}
</table>
</div>
{include file="footer.tpl" noSidebar="true" noAds="true"}