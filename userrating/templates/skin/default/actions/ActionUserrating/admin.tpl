{include file="header.tpl" noSidebar="true"}
<div>
<table border=1>
<tr>
	<th>Login</th>
	<th>Перегляд</th>
	<th>k_Перегляд</th>
	<th>Коментар</th>
	<th>k_Коментар</th>
	<th>Голос</th>
	<th>kГолос</th>
	<th>Базова кількість балів</th>
	<th>Рейтинг</th>
	<th>k_Рейтинг</th>
	<th>Цінність</th>
	<th>%</th>
</tr>
{$totalSum};
{foreach from=$aUserStats item=oUser}
<tr>
	<td>{$oUser['user']->getLogin()}</td>
	<td>{$oUser['views']}</td>
	<td>{$oUser['kViews']}</td>
	<td>{$oUser['comments']}</td>
	<td>{$oUser['kComments']}</td>
	<td>{$oUser['votes']}</td>
	<td>{$oUser['kVotes']}</td>
	<td>{$oUser['totalRating']}</td>
	<td>{$oUser['user']->getRating()}</td>
	<td>{$oUser['kRating']}</td>
	<td>{$oUser['norm_rating']}</td>
	<td>{(100*$oUser['norm_rating']/$totalSum)|string_format:'%.2f'}</td>
</tr>
{/foreach}
</table>
</div>
{include file="footer.tpl" noSidebar="true"}