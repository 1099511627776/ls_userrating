<?php
/*-------------------------------------------------------
*
*   LiveStreet Engine Social Networking
*   Copyright © 2008 Mzhelskiy Maxim
*
*--------------------------------------------------------
*
*   Official site: www.livestreet.ru
*   Contact e-mail: rus.engine@gmail.com
*
*   GNU General Public License, version 2:
*   http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
*
---------------------------------------------------------
*/

$sDirRoot=dirname(dirname(dirname(__FILE__)));
set_include_path(get_include_path().PATH_SEPARATOR.$sDirRoot);
chdir($sDirRoot);

require_once($sDirRoot."/config/loader.php");
require_once($sDirRoot."/engine/classes/Cron.class.php");

class FixRatingCron extends Cron {
	/**
	 * Выбираем пул заданий и рассылаем по ним e-mail
	 */
	public function Client() {
		$aTopics = $this->Topic_GetFixableTopics();
		foreach($aTopics as $oTopic){
			$this->PluginUserrating_ModuleUserrating_updateTopicRating($oTopic,true);
			$this->Log("Fixed rating of topic : ".$oTopic->getId());
		}
	}
}

$sLockFilePath=Config::Get('sys.cache.dir').'fix_rating.lock';
/**
 * Создаем объект крон-процесса, 
 * передавая параметром путь к лок-файлу
 */
$app=new FixRatingCron($sLockFilePath);
print $app->Exec();
?>