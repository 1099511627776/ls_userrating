<?php
class PluginUserrating_ActionUserrating extends ActionPlugin {

	public function Init() {
	}

	protected function RegisterEvent() {
		$this->AddEventPreg('/^admin$/i','/^users$/i','EventAdmin');
		$this->AddEventPreg('/^admin$/i','/^topics$/i','EventAdmin');
	}

	protected function EventAdmin() {
		/**
		 * ���� ������������ �� ����������� � �� �����, �� ���������� ���
		 */
		$this->oUserCurrent=$this->User_GetUserCurrent();
		if (!$this->oUserCurrent or !$this->oUserCurrent->isAdministrator()) {			
			return $this->EventNotFound();
		}
		
		/**
		 * ��������� �������� ����� ���������
		 */
		$action = $this->getParam(0);
		if($action == 'users'){
			if(($fromDate = getRequest('fromDate')) && ($toDate = getRequest('toDate'))){
				$oUserStats = $this->PluginUserrating_ModuleUserrating_getUserStats($fromDate,$toDate);
				$this->Viewer_Assign('aUserStats',$oUserStats['users']);
				$this->Viewer_Assign('totalSum',$oUserStats['totalSum']);
				$this->SetTemplateAction('users');
			} else {
				$this->Message_AddNotice('Error no Dates found',$this->Lang_Get('attention'));
				$this->SetTemplateAction('users');
			}
		} elseif($action == 'topics'){
			$oTopicStats = $this->PluginUserrating_ModuleUserrating_getTopicStats();
			$this->Viewer_Assign('aTopicStats',$oTopicStats['topics']);
			$this->Viewer_Assign('totalSum',$oTopicStats['totalSum']);
			$this->SetTemplateAction('topics');
		}


	}

}
?>