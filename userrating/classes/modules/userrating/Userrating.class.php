<?php

/* -------------------------------------------------------
 *
 *   LiveStreet (v1.0)
 *   Copyright � 2012 1099511627776@mail.ru
 *
 * --------------------------------------------------------
 *
 *   Contact e-mail: 1099511627776@mail.ru
 *
  ---------------------------------------------------------
*/
function sort_by_rating($a,$b){
	return $a['norm_rating_fix'] < $b['norm_rating_fix'];
}

class PluginUserrating_ModuleUserrating extends Module
{
    protected $oRatingMapper;

	public function Init(){
		$conn = $this->Database_GetConnect();
		$this->oRatingMapper = Engine::GetMapper(__CLASS__, 'Userrating', $conn);
	}                       	

	public function upView($oTopic){
		return $this->oRatingMapper->upView($oTopic);
	}

	public function upComment($oTopic){
		return $this->oRatingMapper->upComment($oTopic);
	}

	public function upVote($oTopic){
		return $this->oRatingMapper->upVote($oTopic);
	}

	public function downView($oTopic){
		return $this->oRatingMapper->downView($oTopic);
	}

	public function downComment($oTopic){
		return $this->oRatingMapper->downComment($oTopic);
	}

	public function downVote($oTopic){
		return $this->oRatingMapper->downVote($oTopic);
	}

	public function getTopicStats($iPage=1,$iPerPage=30){
	    $aFilter = array();
	    $count = 0;
		$aTopicStats = $this->oRatingMapper->getTopicStats($aFilter,array(
			'view_koeff' => Config::Get('plugin.userrating.view_koeff'),
			'comment_koeff' => Config::Get('plugin.userrating.comment_koeff'),
			'vote_koeff' => Config::Get('plugin.userrating.vote_koeff'),
			'rating_koeff' => Config::Get('plugin.userrating.rating_koeff'),
		),$count,$iPage,$iPerPage);
		$iTotalSum = 0;
		foreach($aTopicStats as &$rec){
			$oTopic = $this->Topic_GetTopicById($rec['topic']);
			$rec['topic'] = $oTopic;
		}
		return array(
			'topics' => $aTopicStats,
			'totalSum' => $iTotalSum
		);
	}
	public function getUserStats($fromDate,$toDate){
		$aUserStats = $this->oRatingMapper->getUserStats($fromDate,$toDate);
		$excludes = Config::Get('plugin.userrating.excludes');
		$iTotalSum = 0;
		$aUserStatsNew = array();
		foreach($aUserStats as $rec){
			if($oUser = $this->User_GetUserById($rec['user'])){
				$rec['user'] = $oUser;
				if(!in_array($oUser->getId(),$excludes)){
					$rating_val = sqrt($rec['norm_rating_fix']);
					$aUserStatsNew[] = array(
						'user' => $oUser,
						'norm_rating_fix' => $rating_val
					);
					$iTotalSum += $rating_val;
				}
			}
		}
		usort($aUserStatsNew,'sort_by_rating');
		return array(
			'users' => $aUserStatsNew,
			'totalSum' => $iTotalSum
		);
	}

	public function getTopicRating($oTopic){
		return $this->oRatingMapper->getNormRating($oTopic);
	}

	protected function countTopicRating($oTopic){
			$aRatings = $this->oRatingMapper->getTopicRatings($oTopic);
			$views_k = $aRatings['views'] * Config::Get('plugin.userrating.view_koeff');
			$comments_k = $aRatings['comments'] * Config::Get('plugin.userrating.comment_koeff');
			$votes_k = $aRatings['votes'] * Config::Get('plugin.userrating.vote_koeff');
			$user_rating = $oTopic->getUser()->getRating()*Config::Get('plugin.userrating.rating_koeff');
			$norm_rating = $views_k + $comments_k + $votes_k;
			$norm_rating = $norm_rating*(1+$user_rating/100);
			return $norm_rating;
	}

	public function correctVoteByVoterRating($oTopic,$iValue,$oUser){
		$vote_koeff = Config::Get('plugin.userrating.vote_koeff');
		$koeff = log(max($oUser->getSkill(),1));
		$rating = $iValue*$vote_koeff*$koeff;
		$this->Logger_SetFileName(Config::Get('sys.logs.file'));
		$this->Logger_Notice("vote_koeff:{$vote_koeff}, skill:{$oUser->getSkill()}, koeff:{$koeff}, rating:{$rating}");
		$this->oRatingMapper->updateTopicCommentSum($oTopic,$rating);		
	}
	public function updateTopicRating($oTopic,$isFixed = false){
		$rating = $this->countTopicRating($oTopic);
		$this->oRatingMapper->updateTopicRating($oTopic,$rating,$isFixed);
	}

	public function getTopicFixedRating($oTopic){
		return $this->oRatingMapper->getFixedRating($oTopic);
	}
}
?>