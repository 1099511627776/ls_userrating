<?php
/* -------------------------------------------------------
 *
 *   LiveStreet (v1.0)
 *   Copyright � 2012 1099511627776@mail.ru
 *
 * --------------------------------------------------------
 *
 *   Contact e-mail: 1099511627776@mail.ru
 *
  ---------------------------------------------------------
*/

class PluginUserrating_ModuleUserrating_MapperUserrating extends Mapper
{

    protected function topicExists($oTopic){
    	$sql = 'SELECT count(topic_id) as "COUNT" FROM '.Config::Get('plugin.userrating.table.gkrating').' WHERE topic_id = ?d';
    	if($aRows = $this->oDb->select($sql,$oTopic->getId())){
    		return ($aRows[0]['COUNT'] > 0);
    	}
    	return false;
    }

	public function upView($oTopic){
		if($this->topicExists($oTopic)){
			$sql = 'UPDATE '.Config::Get('plugin.userrating.table.gkrating')." SET views = views + 1 WHERE topic_id = ?d";
		} else {
			$sql = 'INSERT INTO '.Config::Get('plugin.userrating.table.gkrating')." (topic_id,views) VALUES (?d,1)";
		}
		if($this->oDb->query($sql,$oTopic->getId())){
			return true;			
		} else {
			return false;
		}
	}

	public function upComment($oTopic){
		if($this->topicExists($oTopic)){
			$sql = 'UPDATE '.Config::Get('plugin.userrating.table.gkrating')." SET comments = comments + 1 WHERE topic_id = ?d";
		} else {
			$sql = 'INSERT INTO '.Config::Get('plugin.userrating.table.gkrating')." (topic_id,comments) VALUES (?d,1)";
		}
		if($this->oDb->query($sql,$oTopic->getId())){
			return true;			
		} else {
			return false;
		}
	}

	public function upVote($oTopic){
		if($this->topicExists($oTopic)){
			$sql = 'UPDATE '.Config::Get('plugin.userrating.table.gkrating')." SET votes = votes + 1 WHERE topic_id = ?d";
		} else {
			$sql = 'INSERT INTO '.Config::Get('plugin.userrating.table.gkrating')." (topic_id,votes) VALUES (?d,1)";
		}
		if($this->oDb->query($sql,$oTopic->getId())){
			return true;			
		} else {
			return false;
		}
	}


	public function downView($oTopic){
		if($this->topicExists($oTopic)){
			$sql = 'UPDATE '.Config::Get('plugin.userrating.table.gkrating')." SET views = IF(views = 0, 0, views - 1) WHERE topic_id = ?d";
		} else {
			$sql = 'INSERT INTO '.Config::Get('plugin.userrating.table.gkrating')." (topic_id,views) VALUES (?d,0)";
		}
		if($this->oDb->query($sql,$oTopic->getId())){
			return true;			
		} else {
			return false;
		}
	}

	public function upFav($oTopic){
		if($this->topicExists($oTopic)){
			$sql = 'UPDATE '.Config::Get('plugin.userrating.table.gkrating')." SET favs = favs + 1 WHERE topic_id = ?d";
		} else {
			$sql = 'INSERT INTO '.Config::Get('plugin.userrating.table.gkrating')." (topic_id,votes) VALUES (?d,1)";
		}
		if($this->oDb->query($sql,$oTopic->getId())){
			return true;			
		} else {
			return false;
		}
	}

	public function downFav($oTopic){
		if($this->topicExists($oTopic)){
			$sql = 'UPDATE '.Config::Get('plugin.userrating.table.gkrating')." SET favs = IF(favs = 0, 0, favs - 1) WHERE topic_id = ?d";
		} else {
			$sql = 'INSERT INTO '.Config::Get('plugin.userrating.table.gkrating')." (topic_id,views) VALUES (?d,0)";
		}
		if($this->oDb->query($sql,$oTopic->getId())){
			return true;			
		} else {
			return false;
		}
	}

	public function downComment($oTopic){
		if($this->topicExists($oTopic)){
			$sql = 'UPDATE '.Config::Get('plugin.userrating.table.gkrating')." SET comments = IF(comments = 0, 0, comments - 1) WHERE topic_id = ?d";
		} else {
			$sql = 'INSERT INTO '.Config::Get('plugin.userrating.table.gkrating')." (topic_id,comments) VALUES (?d,0)";
		}
		if($this->oDb->query($sql,$oTopic->getId())){
			return true;			
		} else {
			return false;
		}
	}

	public function downVote($oTopic){
		if($this->topicExists($oTopic)){
			$sql = 'UPDATE '.Config::Get('plugin.userrating.table.gkrating')." SET votes = votes - 1 WHERE topic_id = ?d";
		} else {
			$sql = 'INSERT INTO '.Config::Get('plugin.userrating.table.gkrating')." (topic_id,votes) VALUES (?d,-1)";
		}
		if($this->oDb->query($sql,$oTopic->getId())){
			return true;			
		} else {
			return false;
		}
	}

	public function getUserStats($fromDate,$toDate){
		$sql = 'SELECT 
					t.user_id as "user",
					sum(ur.norm_rating_fix) as "norm_rating_fix"
				FROM '.Config::Get('plugin.userrating.table.gkrating').' ur
					LEFT OUTER JOIN '.Config::Get('db.table.topic').' t on t.topic_id = ur.topic_id
				WHERE t.topic_date_add BETWEEN ? AND ?
				GROUP BY t.user_id';
		$from_date_ts = strtotime($fromDate);
		$from_date = date('Y-m-d',strtotime('-'.Config::Get('acl.vote.topic.limit_time').' seconds',$from_date_ts));
		$to_date_ts = strtotime($toDate);
		$to_date = date('Y-m-d',strtotime('-'.Config::Get('acl.vote.topic.limit_time').' seconds',$to_date_ts));
		print '<pre>';
		print $fromDate.'::'.$from_date.'<br/>';
		print $toDate.'::'.$to_date.'<br/>';
		print '</pre>';
		if($aRows = $this->oDb->select($sql,$from_date,$to_date)){
			return $aRows;
		}
		return false;
	}

	public function getTopicRatings($oTopic){
		$sql = 'SELECT views,comments,votes FROM '.Config::Get('plugin.userrating.table.gkrating').' WHERE topic_id = ?d';
		if($aRows = $this->oDb->select($sql,$oTopic->getId())){
			return $aRows[0];
		} 
		return false;
	}

	public function updateTopicRating($oTopic,$rating,$update_fixed){
			$sql = "UPDATE ".Config::Get('plugin.userrating.table.gkrating');
			if (!$update_fixed) {
				$sql .= " SET norm_rating = ?f";
			} else {
				$sql .= " SET norm_rating_fix = ?f";
			}
			$sql .= " WHERE topic_id = ?d";
			$this->oDb->query($sql,$rating,$oTopic->getId());
	}

	public function getTopicStats($aFilter,$aParams,&$iCount,$iCurrPage,$iPerPage){
		//$sWhere=$this->buildFilter($aFilter);

		if(!isset($aFilter['order'])) {
			$aFilter['order'] = 't.topic_date_add desc';
		}
		if (!is_array($aFilter['order'])) {
			$aFilter['order'] = array($aFilter['order']);
		}

		$sql = 'SELECT 
					t.topic_id as "topic",
					sum(t.views)*'.$aParams['view_koeff'].' as "views",
					sum(t.comments)*'.$aParams['comment_koeff'].' as "comments",
					sum(t.votes)*'.$aParams['vote_koeff'].' as "votes",
					sum(t.views)*'.$aParams['view_koeff'].'+sum(t.comments)*'.$aParams['comment_koeff'].'+sum(t.votes)*'.$aParams['vote_koeff'].' as "totalRating",
					((sum(t.views)*'.$aParams['view_koeff'].'+sum(t.comments)*'.$aParams['comment_koeff'].'+sum(t.votes)*'.$aParams['vote_koeff'].')*(1+('.$aParams['rating_koeff'].'*u.user_rating)/100)) as "norm_rating"
				FROM '.Config::Get('plugin.userrating.table.gkrating').' t
					LEFT OUTER JOIN '.Config::Get('db.table.topic').' tt on tt.topic_id = t.topic_id
					LEFT OUTER JOIN '.Config::Get('db.table.user').' u on u.user_id = tt.user_id
				GROUP BY t.topic_id
				ORDER BY 6 DESC
				LIMIT ?d,?d
				';
		if($aRows = $this->oDb->selectPage($iCount,$sql,($iCurrPage-1)*$iPerPage,$iPerPage)){
			return $aRows;
		}
		return false;
	}

	public function updateTopicCommentSum($oTopic,$fRating){
		$sql = "UPDATE ".Config::Get('plugin.userrating.table.gkrating')." SET comment_sum = comment_sum + ?f WHERE topic_id = ?d";
		$this->oDb->query($sql,floatval($fRating),$oTopic->getId());
	}
	public function getFixedRating($oTopic){
		$sql = 'SELECT norm_rating_fix FROM '.Config::Get('plugin.userrating.table.gkrating').' WHERE topic_id = ?d';
		if($aRows = $this->oDb->select($sql,$oTopic->getId())){
			return floatval($aRows[0]['norm_rating_fix']);
		}
		return false;
	}

	public function getNormRating($oTopic){
		$sql = 'SELECT norm_rating FROM '.Config::Get('plugin.userrating.table.gkrating').' WHERE topic_id = ?d';
		if($aRows = $this->oDb->select($sql,$oTopic->getId())){
			return $aRows[0]['norm_rating'];
		}
		return false;
	}

}
?>