<?php
class PluginUserrating_ModuleTopic_EntityTopic extends PluginUserrating_Inherit_ModuleTopic_EntityTopic
{
    public function Init(){
    	parent::Init();
    }

    public function isFixed(){
		$rating = $this->PluginUserrating_ModuleUserrating_getTopicFixedRating($this);
		return ($rating != 0);
    }

    public function getFixedRating(){
		return round($this->PluginUserrating_ModuleUserrating_getTopicFixedRating($this),2);
    }

    public function getNormRating(){
       	return round($this->PluginUserrating_ModuleUserrating_getTopicRating($this),2);
    }

}

?>
