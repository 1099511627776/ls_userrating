<?php
/*-------------------------------------------------------
*
*   LiveStreet Engine Social Networking
*   Copyright � 2008 Mzhelskiy Maxim
*
*--------------------------------------------------------
*
*   Official site: www.livestreet.ru
*   Contact e-mail: rus.engine@gmail.com
*
*   GNU General Public License, version 2:
*   http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
*
---------------------------------------------------------
*/

/**
 * ������ ������� ��� ������ � ��
 *
 * @package modules.topic
 * @since 1.0
 */
class PluginUserrating_ModuleTopic_MapperTopic extends PluginUserrating_Inherit_ModuleTopic_MapperTopic {

    public function GetFixableTopics($date){
    	$sql = "
    		SELECT 
    			t.topic_id
			FROM ".Config::Get('plugin.userrating.table.gkrating')." gk
				LEFT OUTER JOIN ".Config::Get('db.table.topic')." t on t.topic_id = gk.topic_id
			WHERE t.topic_date_add <= '".$date."' AND gk.norm_rating_fix = 0			                            
			LIMIT 0,100
    	";
    	$aTopics = array();
		if($aRows = $this->oDb->select($sql)){
			dump($sql);
			foreach($aRows as $row){
				$aTopics[] = $row['topic_id'];
			}
			return $aTopics;
		}
		return false;
    }
	public function GetTopicsTop($iCurPage,$iPerPage,&$iCount,$sPeriod){
		$sql = "
			SELECT 
				t.topic_id 
			FROM ".Config::Get('plugin.userrating.table.gkrating')." gk
				LEFT OUTER JOIN ".Config::Get('db.table.topic')." t on t.topic_id = gk.topic_id
			WHERE t.topic_date_add > '".$sPeriod."' AND t.topic_publish = 1
			ORDER BY gk.norm_rating DESC
			LIMIT ?d,?d
			";
		$aTopics = array();
		if($aRows = $this->oDb->selectPage($iCount,$sql,($iCurPage-1)*$iPerPage, $iPerPage)){
			dump($sql);
			foreach($aRows as $row){
				$aTopics[] = $row['topic_id'];
			}
			return $aTopics;
		}
		return false;
	}
}
?>