<?php

class PluginUserrating_ModuleTopic extends PluginUserrating_Inherit_ModuleTopic
{
    protected $oRatingMapper;

	public function Init(){
		parent::Init();
		$conn = $this->Database_GetConnect();
		$this->oRatingMapper = Engine::GetMapper(__CLASS__, 'Topic', $conn);
	}                       	
    
    public function GetFixableTopics(){
		$date = date('Y-m-d H:i:s', time()-Config::Get('acl.vote.topic.limit_time'));
		$aTopics = $this->oRatingMapper->GetFixableTopics($date);
		$aTopics = $this->Topic_GetTopicsAdditionalData($aTopics,null);
    	return $aTopics;
    }
	public function GetTopicsTop($iPage,$iPerPage,$sPeriod=null,$bAddAccessible=true) {
		if (is_numeric($sPeriod)) {
			// количество последних секунд
			$sPeriod=date("Y-m-d H:00:00",time()-$sPeriod);
		}
		$iCount = 0;
		$aTopicIds = $this->oRatingMapper->GetTopicsTop($iPage,$iPerPage,$iCount,$sPeriod);
		$aResult = array(
			'collection' => $this->GetTopicsAdditionalData($aTopicIds,null),
			'count' => $iCount
		);

		return $aResult;
	}
}
?>