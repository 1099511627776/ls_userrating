<?php

class PluginUserrating_HookUserrating extends Hook {

    /*
     * ����������� ������� �� ����
	*/
    public function RegisterHook() {
        $this->AddHook('topic_show', 'incTopicShow',__CLASS__);
        $this->AddHook('comment_add_after', 'incTopicComment',__CLASS__);
        $this->AddHook('comment_delete_after', 'decTopicComment',__CLASS__);
        $this->AddHook('topic_vote_after', 'updateTopicVote',__CLASS__);
    }

    public function incTopicShow($params){
    	$oTopic = $params['oTopic'];
    	$this->PluginUserrating_ModuleUserrating_upView($oTopic);
    	$this->PluginUserrating_ModuleUserrating_updateTopicRating($oTopic);
    }

    public function incTopicComment($params){
    	$oTopic = $params['oTopic'];
    	$this->PluginUserrating_ModuleUserrating_upComment($oTopic);
    }

    public function decTopicComment($params){
		$oComment = $params['oComment'];
		$oTopic  = $oComment->getTarget();
		if($oComment->getDelete()){
	    	$this->PluginUserrating_ModuleUserrating_downComment($oTopic);
		} else {
	    	$this->PluginUserrating_ModuleUserrating_upComment($oTopic);
		}
    }

    public function updateTopicVote($params){
    	$oTopic = $params['oTopic'];
    	$oTopicVote = $params['oTopicVote'];
    	$iValue = $params['VoteValue'];
    	if($iValue == 1){
	    	$this->PluginUserrating_ModuleUserrating_upVote($oTopic);    		
    	} elseif($iValue == -1){
	    	$this->PluginUserrating_ModuleUserrating_downVote($oTopic);    		
    	}    	
    	$this->PluginUserrating_ModuleUserrating_correctVoteByVoterRating($oTopic,$iValue,$this->User_GetUserCurrent());
    }
	
}
?>
