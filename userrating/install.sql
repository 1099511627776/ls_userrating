--
-- SQL, которые надо выполнить движку при активации плагина админом. Вызывается на исполнение ВРУЧНУЮ в /plugins/PluginAbcplugin.class.php в методе Activate()
-- Например:

CREATE TABLE IF NOT EXISTS `prefix_gkrating` (
  `topic_id` int(11) unsigned NOT NULL,
  `views` int(11) unsigned NOT NULL DEFAULT 0,
  `comments` int(11) unsigned NOT NULL DEFAULT 0,
  `votes` DOUBLE NOT NULL DEFAULT 0.00,
  `favs` int(11) unsinged NOT NULL DEFAULT 0,
  `norm_rating` DOUBLE NOT NULL DEFAULT 0.00
  `norm_rating_fix` DOUBLE NOT NULL DEFAULT 0.00
  PRIMARY KEY (`topic_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
